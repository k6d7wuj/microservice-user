<?php

namespace Tests\Functional;

use Slim\Exception\MethodNotAllowedException;
use Slim\Exception\NotFoundException;
use Tests\BaseClasses\ApiTestCase;

/**
 * Class UserAddTest
 * @package Tests\Functional
 */
class UserAddTest extends ApiTestCase
{
    private $method = 'POST';
    private $url = '/user/';

    /**
     * @return array
     */
    public function dataProvider()
    {
        return [
            [
                'username' => '',
                'password' => '',
                400
            ],
            [
                'username' => 'test',
                'password' => '',
                400
            ],
            [
                'username' => '123456789012345678901234567890123456789'
                    . '012345678901234567890123',
                'password' => '',
                400
            ],
            [
                'username' => 'test123',
                'password' => '',
                400
            ],
            [
                'username' => 'test123',
                'password' => '123',
                400
            ],
            [
                'username' => 'test123',
                'password' => 'too_strong_secret',
                200
            ]
        ];
    }

    /**
     * @dataProvider dataProvider
     * @param string $username
     * @param string $password
     * @param int $responseCode
     * @throws MethodNotAllowedException
     * @throws NotFoundException
     */
    public function testAddUserRouter(string $username, string $password, int $responseCode)
    {
        $response = $this->runApp(
            $this->method,
            $this->url,
            [
                'username' => $username,
                'password' => $password
            ]
        );

        $this->assertEquals($responseCode, $response->getStatusCode());

        $stmt = $this->getApp()->getContainer()->get('pdo')->prepare('TRUNCATE `user`');
        $stmt->execute();
    }
}
