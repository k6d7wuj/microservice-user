<?php

namespace App\Components\Interfaces;


interface StringValidatorInterface
{
    public function validate(string $value);
}