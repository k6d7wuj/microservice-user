<?php

namespace App\Components\Validators\UsernameValidator;

use App\Components\Interfaces\StringValidatorInterface;
use App\Components\Models\UserModel;
use App\Exceptions\InvalidUsernameException;
use Exception;
use PDO;

/**
 * Class UsernameStringValidator
 * @package App\Components\Validators\UsernameValidator
 */
class UsernameStringValidator implements StringValidatorInterface
{
    const USERNAME_MIN_LENGTH = 6;
    const USERNAME_MAX_LENGTH = 60;

    /** @var PDO */
    private $pdo;

    /**
     * UsernameStringValidator constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * @param string $username
     * @throws InvalidUsernameException
     * @throws Exception
     */
    public function validate(string $username): void
    {
        if ( ! $this->isAcceptableLength($username) ) {
            throw new InvalidUsernameException('Username is too short.');
        } elseif ( ! $this->isUnique($username) ) {
            throw new InvalidUsernameException('Given username was used by others.');
        }
    }

    /**
     * Method check the $username on length.
     *
     * @param string $username
     * @return bool
     */
    protected function isAcceptableLength(string $username): bool
    {
        if ( empty($username) ) {
            return false;
        }

        $isTooShort = (strlen($username) < self::USERNAME_MIN_LENGTH);
        $isTooLong = (strlen($username) >= self::USERNAME_MAX_LENGTH);
        if ( $isTooShort or $isTooLong ) {
            return false;
        }

        return true;
    }

    /**
     * Check the given $username on unique value.
     *
     * @param string $username
     * @return bool - is given $username unique?
     * @throws Exception
     */
    protected function isUnique(string $username): bool
    {
        $userModel = new UserModel();
        $user = $userModel->findUserByUsername($username);

        if ( isset($user) ) {
            return false;
        }

        return true;
    }
}