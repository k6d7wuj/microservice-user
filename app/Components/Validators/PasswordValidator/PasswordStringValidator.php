<?php

namespace App\Components\Validators\PasswordValidator;


use App\Components\Interfaces\StringValidatorInterface;
use App\Exceptions\InvalidPasswordException;

/**
 * Class PasswordStringValidator
 * @package App\Components\Validators\PasswordValidator
 */
class PasswordStringValidator implements StringValidatorInterface
{
    const PASSWORD_MIN_LENGTH = 8;
    const PASSWORD_MAX_LENGTH = 60;

    /**
     * @param string $value
     * @throws InvalidPasswordException
     */
    public function validate(string $value)
    {
        if ( ! $this->isOptimalLength($value) ) {
            throw new InvalidPasswordException(
                'The password exceeded MIN or MAX values.'
            );
        } elseif ( ! $this->isEnoughEntropy($value) ) {
            throw new InvalidPasswordException(
                'The password is too simple to comply.'
            );
        }
    }

    /**
     * @param string $password
     * @return bool
     */
    protected function isOptimalLength(string $password): bool
    {
        $isTooShort = mb_strlen($password) <= self::PASSWORD_MIN_LENGTH;
        $isTooLong  = mb_strlen($password) >= self::PASSWORD_MAX_LENGTH;
        if ( $isTooLong or $isTooShort ) {
            return false;
        }

        return true;
    }

    /**
     * @TODO need solution based on regex.
     *
     * @param string $password
     * @return bool
     */
    protected function isEnoughEntropy(string $password): bool
    {
        return true;
    }
}

