<?php

namespace App\Components\Models;


use App\Core\BaseClass;
use DateTime;
use Exception;
use PDO;

/**
 * Class UserModel
 * @package App\Components\Models
 */
class UserModel extends BaseClass
{
    /**
     * @param string $username
     * @param string $password
     * @return int - in case of success return the user's ID otherwise NULL.
     * @throws Exception
     */
    public function addUser(string $username, string $password): ?int {
        /** @var PDO $pdo */
        $pdo = $this->getApp()->getContainer()->get('pdo');

        $stmp = $pdo->prepare("
            INSERT INTO user (name, password, created_at, updated_at)
            VALUE (:name, :password, :created_at, :updated_at)
        ");
        $stmp->bindParam('name', $username, PDO::PARAM_STR);
        $hashedPassword = password_hash($password, PASSWORD_BCRYPT);
        $stmp->bindParam(
            'password',
            $hashedPassword,
            PDO::PARAM_STR
        );
        $createdAt = (new DateTime())->format('Y-m-d H:i:s');
        $stmp->bindParam('created_at', $createdAt);
        $updatedAt = (new DateTime())->format('Y-m-d H:i:s');
        $stmp->bindParam('updated_at', $updatedAt);

        $stmp->execute();

        return $pdo->lastInsertId();
    }

    /**
     * Method try to find an username by $username arg.
     *
     * @param string $username
     * @return array|null - return array if found an user otherwise
     *                      NULL will be returned.
     * @throws Exception
     */
    public function findUserByUsername(string $username): ?array
    {
        $query = "SELECT name FROM user WHERE name = :name";
        $stmt = $this->getPdo()->prepare($query);
        $stmt->bindParam('name', $username, PDO::PARAM_STR);
        $stmt->execute();
        $result = $stmt->fetch(PDO::FETCH_ASSOC);

        if ( empty($result['name']) ) {
            return null;
        }

        return $result;
    }
}

