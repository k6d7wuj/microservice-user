<?php

namespace App\Core;


use Slim\Http\Response;

/**
 * Class BaseController
 * @package App\Core
 */
class BaseController extends BaseClass
{
    /**
     * @param Response $response
     * @param string $body
     * @return Response
     */
    protected function sendBadRequest(Response $response, string $body='Bad Request.')
    {
        $response->withStatus(400)->write($body);

        return $response;
    }
}