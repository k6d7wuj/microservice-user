<?php

namespace App\Core;


use Exception;
use Monolog\Logger;
use PDO;
use Psr\Container\ContainerInterface;
use Slim\App;


/**
 * Class BaseClass
 *
 * Basic class for all controllers in the app.
 *
 * @package App\Core
 */
class BaseClass
{

    /**
     * @return App
     */
    public function getApp(): App
    {
        return $GLOBALS['app'];
    }

    /**
     * @return ContainerInterface
     * @deprecated
     */
    protected function getContainer(): ContainerInterface
    {
        return $this->getApp()->getContainer();
    }

    /**
     * Method check that container contains PDO dependency and return it.
     *
     * @return PDO
     * @throws Exception
     */
    protected function getPdo(): PDO
    {
        if ( $this->getApp()->getContainer()->has('pdo') ) {
            /** @var PDO $pdo */
            $pdo = $this->getContainer()->get('pdo');

            if ( $pdo instanceof PDO ) {
                return $pdo;
            }

            throw new Exception('pdo dependency is not instance of PDO object.');
        }

        throw new Exception('PDO dependency not found in container');
    }

    protected function getLogger(): Logger
    {
        return $this->getApp()->getContainer()->get('logger');
    }
}

