<?php

namespace App\Controllers;

use App\Core\BaseClass;
use Doctrine\ORM\EntityManager;
use PDO;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class HomeController
 *
 * Home controller for basic pages e.g. index, about page and etc.
 *
 * @package App\Controllers
 */
class HomeController extends BaseClass
{
    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function index(Request $request, Response $response, array $args=[]): Response
    {
        $response->write('Hi mark.');
        return $response;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     */
    public function status(Request $request, Response $response, array $args=[]): Response
    {
        /** @var PDO $pdo */
        $pdo = $this->getApp()->getContainer()->get('pdo');

        if ( ! $pdo instanceof PDO ) {
            $response->withStatus(500)->write('Not OK boi. Fix it right now.');
            return $response;
        }

        $response->withStatus(200)->write('OK.');
        return $response;
    }
}

