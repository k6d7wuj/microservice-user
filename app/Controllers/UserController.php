<?php

namespace App\Controllers;


use App\Components\Models\UserModel;
use App\Components\Validators\PasswordValidator\PasswordStringValidator;
use App\Components\Validators\UsernameValidator\UsernameStringValidator;
use App\Core\BaseClass;
use App\Exceptions\InvalidPasswordException;
use App\Exceptions\InvalidUsernameException;
use Exception;
use Slim\Http\Request;
use Slim\Http\Response;

/**
 * Class UserController
 *
 * Controller for register/auth/edit and retrieve info about user.
 *
 * @package App\Controllers
 */
class UserController extends BaseClass
{
    /**
     * Method trying to register an user by given POST body of the request.
     *
     * @param Request $request
     * @param Response $response
     * @param array $args
     * @return Response
     * @throws Exception
     */
    public function register(Request $request, Response $response, array $args=[])
    {
        $data = $request->getParsedBody();
        $username = filter_var($data['username'], FILTER_SANITIZE_STRING);
        $password = filter_var($data['password'], FILTER_SANITIZE_STRING);

        try {
            $usernameValidator = new UsernameStringValidator($this->getPdo());
            $usernameValidator->validate($username);

            $passwordValidator = new PasswordStringValidator();
            $passwordValidator->validate($password);
        } catch (InvalidUsernameException $exception) {
            return $response->withStatus(400)->write($exception->getMessage());
        } catch (InvalidPasswordException $exception) {
            return $response->withStatus(400)->write($exception->getMessage());
        }

        $userModel = new UserModel();

        $userId = $userModel->addUser($username, $password);

        $response->withStatus(200)->write($userId);
        return $response;
    }
}


