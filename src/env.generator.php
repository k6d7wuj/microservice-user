<?php

$envFileName = __DIR__ . '/.env';

$variableNames = [
    'DEVELOPMENT',
    'DISPLAY_ERROR',
    'ADD_CONTENT_LENGTH_HEADER',
    'PRODUCTION_DB_HOST',
    'PRODUCTION_DB_USER',
    'PRODUCTION_DB_PASS',
    'PRODUCTION_DB_NAME',
    'TESTING_DB_HOST',
    'TESTING_DB_USER',
    'TESTING_DB_PASSWORD',
    'TESTING_DB_NAME',
    'USER_LOGGER_NAME'
];

$variables = [];

foreach ($variableNames as $name) {
    $variables[$name] = exec('echo $' . $name);
}

$variables['PROJECT_ROOT'] = "'" . realpath(__DIR__ . '/../../') . "/'";

$content = '';

foreach ($variables as $name => $value) {
    $content .= "$name=$value\n";
}

file_put_contents($envFileName, $content);
