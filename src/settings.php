<?php

use Monolog\Logger;

$dotenv = Dotenv\Dotenv::create(__DIR__);
$dotenv->load();

return [
    'settings' => [
        'development' => getenv('DEVELOPMENT'),
        'displayErrorDetails' => getenv('DISPLAY_ERROR'), // set to false in production
        'addContentLengthHeader' => getenv('ADD_CONTENT_LENGTH_HEADER'), // Allow the web server to send the content-length header

        'db' => [
            'host' => getenv('PRODUCTION_DB_HOST'),
            'user' => getenv('PRODUCTION_DB_USER'),
            'pass' => getenv('PRODUCTION_DB_PASS'),
            'dbname' => getenv('PRODUCTION_DB_NAME')
        ],

        'dbTest' => [
            'host' => getenv('TESTING_DB_HOST'),
            'user' => getenv('TESTING_DB_USER'),
            'pass' => getenv('TESTING_DB_PASSWORD'),
            'dbname' => getenv('TESTING_DB_NAME')
        ],

        // Monolog settings
        'logger' => [
            'name' => getenv('USER_LOGGER_NAME'),
            'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
            'level' => Logger::DEBUG,
        ],
    ],
];
