<?php

use App\Controllers\HomeController;
use App\Controllers\UserController;

$app->get('/hello/[{name}]', HomeController::class . ':index');
$app->get('/status/', HomeController::class . ':status');
$app->post('/user/', UserController::class . ':register')
    ->setName('registration');


