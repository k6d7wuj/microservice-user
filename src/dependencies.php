<?php
// DIC configuration

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

$container = $app->getContainer();

// view renderer
$container['renderer'] = function ($c) {
    $settings = $c->get('settings')['renderer'];
    return new Slim\Views\PhpRenderer($settings['template_path']);
};

// monolog
$container['logger'] = function ($c) {
    $settings = $c->get('settings')['logger'];
    $logger = new Monolog\Logger($settings['name']);
    $logger->pushProcessor(new Monolog\Processor\UidProcessor());
    $logger->pushHandler(new Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
    return $logger;
};

// PDO connection
$container['pdo'] = function ($c) {
    $db = $c->get('settings')['db'];
    $pdo = new PDO('mysql:host=' . $db['host'] . ';dbname=' . $db['dbname'], $db['user'], $db['pass']);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);

    return $pdo;
};

//$container['doctrine'] = function ($c) {
//    $db = $c->get('settings')['db'];
//    $doctrineSettings = $c->get('settings')['doctrine'];
//
//    $paths = $doctrineSettings['entityDirectory'];
//    $isDevMode = $c->get('settings')['development'];
//
//
//
//    $dbParams = array(
//        'dbname' => $db['dbname'],
//        'user' => $db['user'],
//        'password' => $db['pass'],
//        'host' => $db['host'],
//        'driver' => 'pdo_mysql'
//    );
//
//    $config = Setup::createAnnotationMetadataConfiguration($paths, $isDevMode);
//    return EntityManager::create($dbParams, $config);
//};

