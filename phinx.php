<?php

require __DIR__ . '/vendor/autoload.php';

$settings = (include 'src/settings.php')['settings'];

if ( $settings['development'] ) {
    var_dump($settings);
}

return [
    'paths' => [
        'migrations' => '%%PHINX_CONFIG_DIR%%/db/migrations',
        'seeds' => '%%PHINX_CONFIG_DIR%%/db/migrations/seeds'
    ],

    'environments' => [
        'default_migration_table' => 'phinx_log',
        'default_database' => 'development',
        'development' => [
            'adapter'  => 'mysql',
            'host' => $settings['db']['host'],
            'name' => $settings['db']['dbname'],
            'user' => $settings['db']['user'],
            'pass' => $settings['db']['pass']
        ],
        'testing' => [
            'adapter'  => 'mysql',
            'host' => $settings['dbTest']['host'],
            'name' => $settings['dbTest']['dbname'],
            'user' => $settings['dbTest']['user'],
            'pass' => $settings['dbTest']['pass']
        ]
    ],

    'version_order' => 'creation'
];
