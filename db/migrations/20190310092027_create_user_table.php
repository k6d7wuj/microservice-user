<?php


use Phinx\Migration\AbstractMigration;
use Phinx\Util\Literal;

/**
 * Class CreateUserTable
 *
 * Migration create the user table and nothing more.
 */
class CreateUserTable extends AbstractMigration
{
    /**
     * We use change() 'cause Phinx can rollback create statement.
     */
    public function change()
    {
        $this->table('user')
            ->addColumn('name', 'string', [
                'comment' => 'Username(login) for user.',
                'limit' => 60,
                'null' => false
            ])
            ->addColumn('password', 'string', [
                'comment' => 'Password of user\'s account.',
                'limit' => 255,
                'null' => false
            ])
            ->addColumn('created_at', 'timestamp', [
                'comment' => 'The moment when account was created and new hero born',
                'default' => Literal::from('now()'),
                'null' => false
            ])
            ->addColumn('updated_at', 'timestamp', [
                'comment' => 'The time of last update action upon user.',
                'default' => Literal::from('now()'),
                'null' => false
            ])
            ->create();
    }
}
